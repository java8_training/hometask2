import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class CommonFunctions {
    public List<Product> setProductsList() {
        List<Product> productsList = new ArrayList<>();
        productsList.add(new Product("PS5", 40000, "Electronics", "New"));
        productsList.add(new Product("XBox Series S", 35000, "Electronics", "Used"));
        productsList.add(new Product("iPhone", 80000, "Mobile", "New"));
        productsList.add(new Product("Lucky Luke", 100, "Comics", "New"));
        productsList.add(new Product("Charger", 50, "Electronics", "New"));
        productsList.add(new Product("Alexa", 1000, "Electronics", "New"));
        return productsList;
    }

    public List<Response> setResponseList() {
        List<Response> responseList = new ArrayList<>();
        responseList.add(new Response(200, "JSON", "GET Request successful"));
        responseList.add(new Response(400, "JSON", "GET Request failed"));
        responseList.add(new Response(200, "XML", "GET Request successful"));
        responseList.add(new Response(400, "XML", "GET Request failed"));
        responseList.add(new Response(200, "JSON", "PUT Request successful"));
        responseList.add(new Response(400, "JSON", "PUT Request failed"));
        return responseList;
    }

    public void printFilteredProductsList(Predicate<Product> predicateToFilter, List<Product> productsList,String filterDetails){
        System.out.println("\n"+filterDetails);
        for (Product p : productsList) {
            if(predicateToFilter.test(p))
                System.out.println(p);
       }
    }

    public void printFilteredResponseList(Predicate<Response> predicateToFilter, List<Response> responseList,String filterDetails){
        System.out.println("\n"+filterDetails);
        for (Response r : responseList) {
            if(predicateToFilter.test(r))
                System.out.println(r);
        }
    }
}
