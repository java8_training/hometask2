import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main {
    static List<Product> productsList = new ArrayList<>();
    static List<Response> responseList = new ArrayList<>();

    public static void main(String[] args) {
        CommonFunctions commonFunctions = new CommonFunctions();
        productsList = commonFunctions.setProductsList();
        responseList = commonFunctions.setResponseList();

        //Predicate 1 - check if the price of given product is greater than 1000/-.
        Predicate<Product> predicatePriceCheck = product -> product.productPrice > 1000;
        commonFunctions.printFilteredProductsList(predicatePriceCheck,productsList,"Predicate 1 - Products price greater than 1000");

        //Predicate 2 - check if the product is of electronics category.
        Predicate<Product> predicateCategoryCheck = product -> product.productCategory.equalsIgnoreCase("Electronics");
        commonFunctions.printFilteredProductsList(predicateCategoryCheck,productsList,"Predicate 2 - Products belonging to Electronics category");

        //Predicate 3 - check if the price of given product is greater than 100/- and electronics category.
        Predicate<Product> predicatePriceCheck2 = product -> product.productPrice > 100;
        Predicate<Product> combinedPredicate = predicatePriceCheck2.and(predicateCategoryCheck);
        commonFunctions.printFilteredProductsList(combinedPredicate,productsList,"Predicate 3 - Products price greater than 100 and belonging to Electronics category");

        //Predicate 4 - check if the price of given product is greater than 100/- or electronics category.
        Predicate<Product> combinedPredicate2 = predicatePriceCheck2.or(predicateCategoryCheck);
        commonFunctions.printFilteredProductsList(combinedPredicate2,productsList,"Predicate 4 - Products price greater than 100 or belonging to Electronics category");

        //Predicate 5 - check if the price of given product is less than 100/- and in electronics category.
        Predicate<Product> combinedPredicate3 = predicatePriceCheck2.negate().and(predicateCategoryCheck);
        commonFunctions.printFilteredProductsList(combinedPredicate3,productsList,"Predicate 5 - Products price less than 100 and belonging to Electronics category");

        //Predicate 6 - check if the status code is 400.
        Predicate<Response> predicateStatusCodeCheck = response -> response.responseStatusCode == 400;
        commonFunctions.printFilteredResponseList(predicateStatusCodeCheck,responseList,"Predicate 6 - Responses with status code 400");

        //Predicate 7 - check if the response type is JSON.
        Predicate<Response> predicateResponseTypeCheck = response -> response.responseType.equalsIgnoreCase("JSON");
        commonFunctions.printFilteredResponseList(predicateResponseTypeCheck,responseList,"Predicate 7 - Responses with response type JSON");

        //Predicate 8 - check if the status code is 400 and response type is JSON.
        Predicate<Response> predicateCombinedCheck = predicateStatusCodeCheck.and(predicateResponseTypeCheck);
        commonFunctions.printFilteredResponseList(predicateCombinedCheck,responseList,"Predicate 8 - Responses with status code 400 and response type JSON");

        //Predicate 9 - check if the status code is 400 or response type is JSON.
        Predicate<Response> predicateCombinedCheck2 = predicateStatusCodeCheck.or(predicateResponseTypeCheck);
        commonFunctions.printFilteredResponseList(predicateCombinedCheck2,responseList,"Predicate 9 - Responses with status code 400 or response type JSON");

        //Predicate 10 - check if the status code is not 400 and response type is JSON.
        Predicate<Response> predicateCombinedCheck3 = predicateStatusCodeCheck.negate().and(predicateResponseTypeCheck);
        commonFunctions.printFilteredResponseList(predicateCombinedCheck3,responseList,"Predicate 10 - Responses without status code 400 and response type JSON");

    }
}