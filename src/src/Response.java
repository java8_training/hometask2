public class Response {
    String responseBody, responseType;
    int responseStatusCode;

    Response(int responseStatusCode, String responseType, String responseBody){
        this.responseStatusCode = responseStatusCode;
        this.responseType = responseType;
        this.responseBody = responseBody;
    }

    @Override
    public String toString(){
        return "Response Status code : "+responseStatusCode+"\tResponse type : "+responseType+"\tResponse body : "+responseBody;
    }

}
