# HomeTask2

# Description

# Pre Conditions
- Define a Product class with name, price, category, grade
- Define a Response class with response body, status code, response type

# Predicates

1. Define a predicate to check if the price of given product is greater than 1000/-. Print all the products from the given list of the products if the price is greater than 1000/-.
2. Define a predicate to check if the product is of electronics category. Print all the products from the given list of the products if the product is of electronics category.
3. Print all the products from the given list of product if the product price is greater than 100/- which are in electronics category.
4. Print all the products from the given list of product if the product price is greater than 100/- or product is in electronics category.
5. Print all the products from the given list of product if the product price is less than 100/- and product is in electronics category.
6. Define a predicate to check if the status code is 400. Print all the responses with status code 400 from the given list of responses.
7. Define a predicate to check if the response type JSON. Print all the responses the response type JSON from the given list of responses.
8. Print all the responses with status code 400 and response type JSON
9. Print all the responses with status code 400 or response type JSON
10. Print all the responses with status code is not 400 and response type JSON

# JAVA Classes
Product and Response - As mentioned in the precondition
CommonFunctions - Class to initialise list of Products and Responses and to print the filtered results after applying the predicates
Main - Function calls to create the list, create the predicates and call the print functions